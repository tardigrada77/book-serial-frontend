import Vue from 'vue'
import App from './App.vue'
import store from './store'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import router from './router'


Vue.config.productionTip = false

Vue.use(Buefy)

new Vue({
  router,
  store,
  created(){
    // TODO: Переписать всю логику
    // Должно при обновлении проверять просрочен ли токен
    // Если нет получать данные пользователя заново
    // Либо хранить их в indexedDB
    if(localStorage['token']){

      let token = JSON.parse(atob(localStorage['token'].split('.')[0]))
      let currentTime = Date.now();
      let expire_in = token.exp * 1000;

      if(expire_in > currentTime){
        this.$store.dispatch('REFRESH', localStorage['token'])
      } else if (expire_in <= currentTime) {
        this.$store.commit('SIGNOUT');
      }
    }
  },
  render: h => h(App)
}).$mount('#app')
