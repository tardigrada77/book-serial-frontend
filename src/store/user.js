import axios from 'axios'

export default {
    state: {
        user: {
            isAuthenticated: false,
            username: null,
            token: null,
            avatar: null,
            uri: null
        }
    },
    mutations: {
        SET_USER(state, payload){
            state.user.isAuthenticated = true
            state.user.token = localStorage['token']
            state.user.avatar = payload.avatar
            state.user.username = payload.username
            state.user.uri = payload.uri
        },
        SIGNOUT(state){
            state.user = {
                isAuthenticated: false,
                username: null,
                token: null
            }
            localStorage.removeItem('token');
        }
    },
    actions: {
        SIGNUP({commit, dispatch}, payload){
            axios.request({
                url: '/api/users',
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                },
                data: {
                    username: payload.login,
                    password: payload.password
                }
            }).then(response => {
                    console.log('Зарегистрирован')
                    let user = {
                        login: payload.login,
                        password: payload.password
                    }
                    dispatch('SIGNIN', user)
            }).catch(error => {
                if (error.response.status == 400){
                    commit('SET_ERROR', 'Такой пользователь уже существует')
                } else if (error.response.status == 500){
                    commit('SET_ERROR', 'Внутренняя ошибка сервера')
                }

            })
        },
        SIGNIN({commit, dispatch}, payload){
            let auth = window.btoa(`${payload.login}:${payload.password}`)
            axios.request({
                url: '/api/token',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With': 'XMLHttpRequest',
                    'Authorization': `Basic ${auth}`,
                }
            }).then(response => {
                    console.log(response.status)
                    localStorage['token'] = response.data.token
                    dispatch('REFRESH', localStorage['token'])
            }).catch(error => {
                if (error.response.status == 400){
                    commit('SET_ERROR', 'Неправильный логин или пароль')
                } else if (error.response.status == 500){
                    commit('SET_ERROR', 'Внутренняя ошибка сервера')
                }

            })
        },
        REFRESH({commit}, payload){
            let id = JSON.parse(atob(payload.split('.')[1]))
            let up = window.btoa(`${payload}:qwerty`)
            axios.request({
                url: `/api/users/${id.id}`,
                headers: {
                    'Authorization': `Basic ${up}`,
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(response => {
                console.log(response.data)
                commit('SET_USER', response.data.user)
            })
        }
    },
    getters: {
        getUserAuth: (state) => state.user.isAuthenticated,
        getUserData: (state) => state.user
    }
}