import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Studios from './views/Studios.vue'
import Serials from './views/Serials.vue'
import Profile from './views/Profile.vue'
import News from './views/News.vue'
import Subscriptions from './views/Subscriptions.vue'
import Settings from './views/Settings.vue'
import Reader from './views/Reader.vue'
import Error404 from './views/404.vue'
import InternalError from './views/InternalError.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },  
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    },
    {
      path: '/subscriptions',
      name: 'subscriptions',
      component: Subscriptions
    },
    {
      path: '/studios',
      name: 'studios',
      component: Studios
    },
    {
      path: '/serials',
      name: 'serials',
      component: Serials
    },
    {
      path: '/news',
      name: 'news',
      component: News
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings
    },
    {
      path: '/reader',
      name: 'reader',
      component: Reader
    },
    {
      path: '/testError',
      name: 'error',
      component: InternalError
    }
  ],
  mode: 'history'
})
